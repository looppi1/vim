# README #

# How do I get set up? #

## Clone repositories ##
git clone this repo to your home folder and then:

**git submodule init**

**git submodule update**


## Configure stuff ##

This is happening in your home folder:

**mv vim .vim/**

**ln -s .vim/vimrc .vimrc**

in .vim folder make directories:

**mkdir tmp**

**mkdir tmp/swap**

**mkdir tmp/backup**


## Enjoy ##

you're done \:D/


# Usage #

:NERDTree to open nerdtree

, + hjkl to move active tab.

:bd to close buffer (tab for example)
or
:q to quit

## Open Multiple Files Split Screen ##

**Opens ctrlp:**
ctrl + p 

**Open with Vertical Split:**
ctrl + v

**Open with Horizontal Split:**
ctrl + x

**Switch view in ctrlp:**
ctrl + f

## In NERDTree ##


**Open with Vertical split:** s

**Open with Horizontal split:** i