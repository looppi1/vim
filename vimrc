set nocompatible

" Pathogen
call pathogen#infect()
call pathogen#helptags()

"set statusline=%<\ %n:%f\ %m%r%y%=%-35.(line:\ %l\ of\ %L,\ col:\ %c%V\ (%P)%)
" Statusline with Syntastic
" set statusline=%<%f%y\ %#warningmsg#%{SyntasticStatuslineFlag()}%*\ %h%m%r%=%-14.(%l/%L,%c%V%)\ %P
filetype plugin indent on
if has('python3')
  silent! python3 1
endif

silent! python3 from powerline.vim import setup as powerline_setup

call plug#begin('~/.vim/plugged')
Plug '~/.fzf'
Plug 'junegunn/fzf.vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'tpope/vim-surround'
Plug 'elmcast/elm-vim'
Plug 'Valloric/YouCompleteMe'
"-------------------=== Code/Project navigation ===-------------
Plug 'scrooloose/nerdtree'                " Project and file navigation
Plug 'Xuyuanp/nerdtree-git-plugin'        " NerdTree git functionality
" Plug 'majutsushi/tagbar'                  " Class/module browser
" Plug 'vim-ctrlspace/vim-ctrlspace'        " Tabs/Buffers/Fuzzy/Workspaces/Bookmarks
" Plug 'mileszs/ack.vim'                    " Ag/Grep
Plug 'vim-airline/vim-airline'            " Lean & mean status/tabline for vim
Plug 'vim-airline/vim-airline-themes'     " Themes for airline
" Plug 'fisadev/FixedTaskList.vim'          " Pending tasks list
" Plug 'MattesGroeger/vim-bookmarks'        " Bookmarks
" Plug 'thaerkh/vim-indentguides'           " Visual representation of indents
Plug 'w0rp/ale'                           " Async Lint Engine
Plug 'Valloric/YouCompleteMe'             " Code Completion

"-------------------=== Other ===-------------------------------
" Plug 'vimwiki/vimwiki'                    " Personal Wiki
" Plug 'jreybert/vimagit'                   " Git Operations
" Plug 'kien/rainbow_parentheses.vim'       " Rainbow Parentheses
"-------------------=== Snippets support ===--------------------
Plug 'MarcWeber/vim-addon-mw-utils'       " dependencies #1
Plug 'tomtom/tlib_vim'                    " dependencies #2

"-------------------=== Languages support ===-------------------
" Plug 'scrooloose/nerdcommenter'           " Easy code documentation
" Plug 'mitsuhiko/vim-sparkup'              " Sparkup(XML/jinja/htlm-django/etc.) support

"-------------------=== Python  ===-----------------------------
" Plug 'klen/python-mode'                   " Python mode (docs, refactor, lints...)
Plug 'hynek/vim-python-pep8-indent'
Plug 'mitsuhiko/vim-python-combined'
Plug 'mitsuhiko/vim-jinja'
Plug 'jmcantrell/vim-virtualenv'
Plug 'ryanoasis/vim-devicons'             " Dev Icons

call plug#end()

syntax on
filetype on
set encoding=utf8
set number
set mouse=a
set ruler
set ttyfast                                 " terminal accelerationet mousehide
set nolist

set hlsearch
set showmatch
set incsearch
set ignorecase
set autoindent
set history=1000
set cursorline
set completeopt=menu
if has("unnamedplus")
  set clipboard=unnamedplus
elseif has("clipboard")
  set clipboard=unnamed
endif

set expandtab
set shiftwidth=4
set tabstop=4
set softtabstop=4

autocmd BufNewFile,BufRead *.styl set filetype=stylus
autocmd Filetype javascript setlocal ts=2 sw=2 expandtab
autocmd Filetype ruby setlocal ts=2 sw=2 expandtab
autocmd Filetype stylus setlocal ts=4 sw=4 sts=0 noexpandtab
autocmd Filetype jade setlocal ts=4 sw=4 sts=0 noexpandtab
autocmd Filetype yaml setlocal syntax=off ts=2 sw=2 expandtab
autocmd FileType tf setlocal commentstring=#\ %s


" Folding
set foldlevel=9999 " initially open all folds
command FoldAll set foldlevel=0
command FoldOne set foldlevel=1

" Nerdtree
" autocmd VimEnter * NERDTree
autocmd VimEnter * wincmd p
let NERDTreeShowBookmarks=1
let NERDTreeChDirMode=0
let NERDTreeQuitOnOpen=0
let NERDTreeMouseMode=2
let NERDTreeShowHidden=1
let NERDTreeIgnore=['\.pyc','\~$','\.swo$','\.swp$','\.git','\.hg','\.svn','\.bzr']
let NERDTreeKeepTreeInNewTab=1
let g:nerdtree_tabs_open_on_gui_startup=0
 
set t_Co=256
colorscheme Tomorrow-Night

"" Leader mappings
let mapleader = ","
let maplocalleader = ";"

" Show statusline always
set laststatus=2

set title " change the terminal's title
set noerrorbells " don't beep

" Toggle pastemode easily in insert and command mode
set pastetoggle=<F2>

" Some aliases
command W w
command Q q
command WQ wq
command Wq wq
command Qa qa
command QA qa
command Wa wa
command WA wa

" Run flake8 in every save
" autocmd BufWritePost *.py call Flake8()

" Makes Caps Lock work as Esc
command EscToCapsLock !xmodmap -e 'clear Lock' -e 'keycode 0x42 = Escape'

"" Window management

" new vertical split
command Vertical vertical sp

" new horizontal split
command Horizontal sp

" Move by screen lines instead of file line. Nice with long lines.
nnoremap j gj
nnoremap k gk

" Easily move between split windows using <leader>hjkl
nnoremap <leader>h <C-w>h
nnoremap <leader>j <C-w>j
nnoremap <leader>k <C-w>k
nnoremap <leader>l <C-w>l

" Easily resize split windows with Ctrl+hjkl
nnoremap <C-j> <C-w>+
nnoremap <C-k> <C-w>-
nnoremap <C-h> <C-w><
nnoremap <C-l> <C-w>>

" fzf 
nnoremap <Leader>bb :Buffers<CR>
nnoremap <Leader>w? :Windows<CR>
nnoremap <Leader>ff :Files<CR>
nnoremap <Leader>gf :GitFiles<CR>


" Force redraw to C-l
nnoremap <Leader>r :redraw!<CR>

" Python
" """"""""
autocmd BufRead,BufNewFile *.py set cinwords=if,elif,else,for,while,try,except,finally,def,class
autocmd BufWritePre *.py normal m`:%s/\s\+$//e ``
autocmd BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") |
                      \ exe "normal g'\"" | endif   

let Tlist_Ctags_Cmd = "/usr/bin/ctags"
let Tlist_WinWidth = 50
" map <F4> :TlistToggle<cr>
" map <F8> :!/usr/bin/ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<CR>

let g:syntastic_auto_loc_list=1
let g:syntastic_enable_signs=1
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_tex_checkers = ['chktex']


let g:syntastic_mode_map = { 'mode': 'active',
                           \ 'active_filetypes': [],
                           \ 'passive_filetypes': ['python', 'vim','html', 'bib'] }

" Backups
set backupdir=~/.vim/tmp/backup/ " backups
set directory=~/.vim/tmp/swap/ " swap files
set backup " enable backup

set updatetime=1000 
let g:vimchant_spellcheck_lang = 'fi'
let g:airline_powerline_fonts = 1

" let g:ctrlp_cmd = 'CtrlPLastMode'
" let g:ctrlp_extensions = ['line', 'dir']
" unused exts: tag buffertag
